`include "../uvm/my_pkg.sv"
import my_pkg::*;

interface if_lan
    (
        input clk, reset
    );

    logic port_req, port_received;
    logic [my_pkg::DATA_WIDTH - 1 : 0] port_data;

    modport dut (
    input port_received, clk, reset,
    output port_data, port_req
    );

    modport lan_port (
    input port_data, port_req, clk, reset,
    output port_received
    );

endinterface