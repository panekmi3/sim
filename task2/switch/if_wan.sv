`include "../uvm/my_pkg.sv"
import my_pkg::*;

interface if_wan 
    (
        input logic clk, reset
    );
    logic [my_pkg::DATA_WIDTH - 1 : 0] packet_data;
    logic [my_pkg::PORT_ADDR_LENGTH - 1 : 0] packet_addr;
    logic packet_send_req;

    logic [$clog2(my_pkg::NUM_OF_PORTS) - 1 : 0] mem_port_index;
    logic mem_write_enable;

    logic packet_finished;

    modport dut (
    input packet_addr, packet_data, packet_send_req, mem_port_index, mem_write_enable,
    output packet_finished
    );

    modport wan (
    input packet_finished,
    output packet_addr, packet_data, packet_send_req, mem_port_index, mem_write_enable
    ); 

endinterface //if_wan