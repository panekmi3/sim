`include "if_wan.sv"
`include "if_lan.sv"

module switch 
#(
  parameter NUM_OF_PORTS = 5, PORT_ADDR_LENGTH = 3, DATA_WIDTH = 8
)
(
  input wire clk, reset,
  if_wan.dut interface_input,
  if_lan.dut interface_output [NUM_OF_PORTS - 1 : 0]
);

wire port_found, received, packet_send, ready;
wire [NUM_OF_PORTS - 1 : 0] eq;
wire [NUM_OF_PORTS * PORT_ADDR_LENGTH - 1:0] data;


//signals for unpacked interface
wire [NUM_OF_PORTS - 1 : 0 ] port_req, port_received;
wire [NUM_OF_PORTS * DATA_WIDTH - 1 : 0] port_data;

//interface unpacking
for (genvar i = 0; i < NUM_OF_PORTS; i++) begin
  assign interface_output[i].port_req = port_req[i];
  assign port_received[i] = interface_output[i].port_received;
  assign interface_output[i].port_data = port_data[ i * DATA_WIDTH +: DATA_WIDTH] ;
end

assign port_found = |eq;

//assign interface_input.mem_data = data;

controller # (
    .NUM_OF_PORTS(NUM_OF_PORTS),
    .PORT_ADDR_LENGTH(PORT_ADDR_LENGTH),
    .DATA_WIDTH(DATA_WIDTH)
  )
  controller_inst (
    .clk(clk),
    .reset(reset),
    .packet_send_req(interface_input.packet_send_req),
    .port_found(port_found),
    .received(received),
    .packet_finished(interface_input.packet_finished),
    .packet_send(packet_send),
    .ready(ready)
  );

memory # (
    .NUM_OF_PORTS(NUM_OF_PORTS),
    .PORT_ADDR_LENGTH(PORT_ADDR_LENGTH),
    .DATA_WIDTH(DATA_WIDTH)
  )
  memory_inst (
    .clk(clk),
    .reset(reset),
    .port_index(interface_input.mem_port_index),
    .port_addr(interface_input.packet_addr),
    .write_enable(interface_input.mem_write_enable),
    .data(data),
    .ready(ready),
    .port_found(port_found)
  );

comparators # (
    .NUM_OF_PORTS(NUM_OF_PORTS),
    .PORT_ADDR_LENGTH(PORT_ADDR_LENGTH),
    .DATA_WIDTH(DATA_WIDTH)
  )
  comparators_inst (
    .packet_addr(interface_input.packet_addr),
    .mem_data_all(data),
    .eq(eq)
  );

mux_demux_logic # (
    .NUM_OF_PORTS(NUM_OF_PORTS),
    .PORT_ADDR_LENGTH(PORT_ADDR_LENGTH),
    .DATA_WIDTH(DATA_WIDTH)
  )
  mux_demux_logic_inst (
    .clk(clk),
    .reset(reset),
    .packet_data(interface_input.packet_data),
    .packet_send(packet_send),
    .eq(eq),
    .received(received),
    
    .port_received(port_received),
    .port_req(port_req),
    .port_data(port_data)
  );

endmodule