module memory
#(
    parameter NUM_OF_PORTS = 5, PORT_ADDR_LENGTH = 3, DATA_WIDTH = 8
)  
(
    input wire clk, reset,
    input wire [$clog2(NUM_OF_PORTS) - 1 : 0] port_index,
    input wire [PORT_ADDR_LENGTH - 1 : 0] port_addr,
    input wire write_enable,
    input wire ready,
    input wire port_found,

    output wire [NUM_OF_PORTS * PORT_ADDR_LENGTH - 1 : 0] data
);

reg [PORT_ADDR_LENGTH - 1 : 0] mem [NUM_OF_PORTS - 1 : 0];

integer i;

always @(posedge clk ) begin
    if (reset) begin
        for (i = 0; i < NUM_OF_PORTS; i = i+1) begin
            mem[i] <= i;
        end
    end

    //enable write only if the controller is in waiting for a new packet and the address is not yet stored
    // deleting "ready" allows to write during sending, breaking test 4
    // deleting "!port_found" allows duplicate addresses to be written, breaking test 5
    else if (write_enable && ready && !port_found) begin
        //address 0x0 is not allowed 
        if (port_addr != 0) begin
            mem[port_index] <= port_addr;  
        end
    end
end

genvar j;
for ( j = 0 ; j < NUM_OF_PORTS ; j=j+1 ) begin
    assign data[PORT_ADDR_LENGTH*j+PORT_ADDR_LENGTH-1:PORT_ADDR_LENGTH*j] = mem[j];
end

initial begin
    for (i = 0; i < NUM_OF_PORTS; i = i+1) begin
        mem[i] <= 0;
    end
end

endmodule