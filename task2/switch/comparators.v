module comparators 
#(
    parameter NUM_OF_PORTS = 5, PORT_ADDR_LENGTH = 3, DATA_WIDTH = 8
)  
(
    input wire [PORT_ADDR_LENGTH - 1 : 0] packet_addr,
    input wire [PORT_ADDR_LENGTH * NUM_OF_PORTS - 1 : 0] mem_data_all,
    output reg [NUM_OF_PORTS - 1 : 0] eq
);

integer i;

always @(*) begin
    for (i = 0; i < NUM_OF_PORTS; i = i+1) begin: OUT
        if (  mem_data_all[i * PORT_ADDR_LENGTH +: PORT_ADDR_LENGTH] == packet_addr && packet_addr != 0 )
            eq[i] <= 1;
        else
            eq[i] <= 0;
    end
end
    
endmodule