`timescale 1ns/10ps

`include "uvm_macros.svh"

// import knihovny UVM
import uvm_pkg::*;

class my_item extends uvm_sequence_item;

  // deklarace jednotlivych polozek
  rand bit [my_pkg::PORT_ADDR_LENGTH-1:0] addr;
  rand bit [my_pkg::DATA_WIDTH-1:0] data;
  rand bit mem_operation;
  rand bit [$clog2(my_pkg::NUM_OF_PORTS)-1:0] port_index;

  rand bit mem_write_while_send;
  
  // omezeni nahodnych hodnot
  constraint constraint_addr {
    //addr inside {[0:my_pkg::NUM_OF_PORTS-1]};
    addr != 0;
    
    port_index inside {[0:my_pkg::NUM_OF_PORTS-1]};

    //memory operations are less likely than packet transfers
    mem_operation dist {1 := 1, 0 := 9};

    mem_write_while_send dist {1 := 1, 0 := 9};

    //do not allow both legal and illegal mem writes 
    (mem_write_while_send == 1) -> (mem_operation == 0);
    (mem_operation == 1) -> (mem_write_while_send == 0); 
  }
  
  // registrace tridy a vsech polozek do factory (dulezite! - musi byt pro kazdou polozku)
  `uvm_object_utils_begin(my_item)
    `uvm_field_int(addr, UVM_DEFAULT)
    `uvm_field_int(data, UVM_DEFAULT)
    `uvm_field_int(mem_operation, UVM_DEFAULT)
    `uvm_field_int(port_index, UVM_DEFAULT)
    `uvm_field_int(mem_write_while_send, UVM_DEFAULT)
//    `uvm_field_int(variable_bit, UVM_DEFAULT) // pro promennou typu bit se take pouziva uvm_field_int
  `uvm_object_utils_end
  
  // konstruktor
  function new (string name = "my_item");
    super.new(name);
  endfunction

endclass
