`timescale 1ns/10ps

`include "uvm_macros.svh"

// import knihovny UVM
import uvm_pkg::*;

class my_scoreboard extends uvm_scoreboard;

  // registrace do factory
  `uvm_component_utils(my_scoreboard)

  // importy pro prijem z jednotlivych monitoru
  uvm_tlm_analysis_fifo #(my_pkg::my_item) inst_in_fifo;
  uvm_tlm_analysis_fifo #(my_pkg::my_item) inst_out_fifo[0:my_pkg::NUM_OF_PORTS-1];
  
  reg [my_pkg::PORT_ADDR_LENGTH - 1 : 0] mem_copy [my_pkg::NUM_OF_PORTS - 1 : 0];

  my_pkg::my_item in_item;
  my_pkg::my_item out_item;

  covergroup cg;
    option.per_instance = 1;
    coverpoint in_item.addr
    {
      //ignore_bins over = {[my_pkg::NUM_OF_PORTS : $]};
      ignore_bins zero = {0};

      //it is more important to check all the addresses than all combinations of data
      option.weight = 5;
    }
    coverpoint in_item.data;

    coverpoint in_item.port_index
    {
      illegal_bins over = {[my_pkg::NUM_OF_PORTS : $]};
      option.weight = 5;
    }

    coverpoint in_item.mem_operation
    {
      //require at least 20 memory writes, kind of a lot, but unsuccessful transactions are counted as well
      option.at_least = 20;
      ignore_bins zero = {0};
    }
  endgroup

  // konstruktor
  function new (string name = "my_scoreboard", uvm_component parent = null);
    super.new(name, parent);
    cg = new();
    for (integer i = 0; i < my_pkg::NUM_OF_PORTS; i = i+1) begin
      mem_copy[i] <= i;
    end
  endfunction

  // faze build
  function void build_phase (uvm_phase phase);
    super.build_phase(phase);
    // vytvoreni portu pro prijem (muze byt i v konstruktoru)
    inst_in_fifo = new("inst_in_fifo", this);
    
    for(int i = 0; i < my_pkg::NUM_OF_PORTS; i++) begin
      inst_out_fifo[i] = new($sformatf("inst_out_fifo[%0d]", i), this);
    end
  endfunction

  // faze run - vlakno, ktere bude vybirat data z jednotlivych monitoru
  task run_phase (uvm_phase phase);
    super.run_phase(phase);
    
    // zamezeni ukonceni testu, dokud neni splneno pokryti
    phase.raise_objection(this);
    
    // vytvoreni samostatnych vlaken - pro kazdy port jedno
    for(int i = 0; i < my_pkg::NUM_OF_PORTS; i++) begin
      fork
        int index = i;
        scoreboard_match(index, phase);
      join_none
    end

    // fork
    //   scoreboard_mem(phase);
    // join_none

    // ekvivalentni kod bez for cyklu
//    fork
//      scoreboard_match(0, phase);
//      scoreboard_match(1, phase);
//      scoreboard_match(2, phase);
//      scoreboard_match(..., phase);
//    join

  endtask

  // task scoreboard_mem(uvm_phase phase);
  //   forever begin

  //     `uvm_info(get_full_name(), $sformatf("memory 1"), UVM_LOW) 
  //     wait( ! inst_in_fifo.is_empty())
  //     inst_in_fifo.peek(in_item_mem);
  //     `uvm_info(get_full_name(), $sformatf("memory 2"), UVM_LOW) 

  //   end
  // endtask


  task scoreboard_match(int index, uvm_phase phase);
    // objekty, do kterych se budou ukladat data z monitoru
    
    //`uvm_info(get_type_name(), $sformatf("Scoreboard: scoreboard_match: start - thread %0d", index), UVM_LOW)

    forever begin
      // blokujici cteni z vystupni fronty (vystupniho rozhrani)
      inst_out_fifo[index].get(out_item);
      // pokud prisla data z vystupniho portu, tak precti data ze vstupu (v sablone tam jina byt nemohou)
      inst_in_fifo.get(in_item);

      cg.sample();

      begin
        int found = 0;
        while ( ! found || in_item.mem_operation) begin 
          
          if( in_item.mem_write_while_send )
          begin
            //ignore transaction
            inst_in_fifo.get(in_item);
            continue;
          end

          //check whether the packet address is in memory, if not ignore the packet
          found = 0;
          for (int i = 0; i < my_pkg::NUM_OF_PORTS; i++) begin
            if (mem_copy[i] == in_item.addr)
              found = 1;
          end

          if(in_item.mem_operation && ! found) begin
            mem_copy[in_item.port_index] = in_item.addr;
            //$display("\n------------------MEMORY CHANGED------------------");
          end

          if ( ! found || in_item.mem_operation)
          begin
            found = 0;
            inst_in_fifo.get(in_item);
          end
        end
      end

      out_item.addr = mem_copy[index];

      

      // porovnani
      if (~in_item.compare(out_item))
        // panika
        `uvm_error(get_full_name(), $sformatf("Scoreboard: scoreboard_match - thread %0d : item mismatch\nin_item: \n%s\nout_item: \n%s", index, in_item.sprint(), out_item.sprint()))
        //`uvm_error(get_full_name(), $sformatf("Scoreboard: scoreboard_match - thread %0d : item mismatch", index))
      //else
        // ok
        //`uvm_info(get_full_name(), $sformatf("Scoreboard: scoreboard_match - thread %0d : item match\nin_item: \n%s\nout_item: \n%s", index, in_item.sprint(), out_item.sprint()), UVM_LOW)
        //`uvm_info(get_full_name(), $sformatf("Scoreboard: scoreboard_match - thread %0d : item match", index), UVM_LOW)
      
      //`uvm_info(get_full_name(), $sformatf("Coverage = %0.2f %%", cg.get_inst_coverage()), UVM_LOW)

      if (cg.get_inst_coverage() >= my_pkg::TARGET_COVER_PERCENTAGE) begin
        `uvm_info(get_full_name(), $sformatf("\n\nCoverage reached, ending test.\n\n"), UVM_LOW)  
        phase.drop_objection(this);
      end
    end
  endtask

  // funkce, do ktere lze vlozit vypis pokryti (i jinou zpravu) po skonceni testu
  function void report_phase(uvm_phase phase);
    `uvm_info(get_full_name(), $sformatf("Coverage = %0.2f %%", cg.get_inst_coverage()), UVM_LOW)
  endfunction

endclass
