package my_pkg;

    parameter NUM_OF_PORTS = 5, PORT_ADDR_LENGTH = 3, DATA_WIDTH = 8, TARGET_COVER_PERCENTAGE = 95;

    `include "my_item.sv" 

    `include "my_in_monitor.sv"

    `include "my_monitor.sv"

    `include "my_driver.sv"

    `include "my_sequencer.sv"

    `include "my_scoreboard.sv"

    `include "my_env.sv"

    `include "my_sequence.sv"

    `include "my_test.sv"

endpackage


