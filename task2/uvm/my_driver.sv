`timescale 1ns/10ps

`include "uvm_macros.svh"

// import knihovny UVM
import uvm_pkg::*;

class my_driver extends uvm_driver #(my_pkg::my_item);
  // zakladni trida uvm_driver je parametrizovatelna
  // parametr je trida definujici transakci
  
  // registrace do factory
  `uvm_component_utils(my_driver)
  
  // deklarace rozhrani DUT
  virtual if_wan inst_if_wan;
  
  // konstruktor
  function new (string name = "my_driver", uvm_component parent = null);
    super.new(name, parent);
  endfunction
  
  // faze build
  virtual function void build_phase (uvm_phase phase);
    super.build_phase(phase);
    // vyzvednuti reference na rozhrani DUT z databaze
    if (!uvm_config_db#(virtual if_wan)::get(this, "", "inst_if_wan", inst_if_wan))
      `uvm_fatal("NOVIF", {"Missing inst_if_wan: ", get_full_name(), ".inst_if_wan"})
  endfunction
  
  // faze run
  task run_phase (uvm_phase phase);
    super.run_phase(phase);
  
    // inicializace
    inst_if_wan.packet_send_req <= 1'b0;
    inst_if_wan.packet_addr <= {my_pkg::PORT_ADDR_LENGTH{1'bz}};
    inst_if_wan.packet_data <= {my_pkg::DATA_WIDTH{1'bz}};

    // cekani na reset
    @(posedge inst_if_wan.reset);

    // nekonecna smycka
    forever begin

      while(inst_if_wan.reset !== 1'b0) begin
        // pokud (dokud) je reset, tak podrzet vychozi hodnoty
        inst_if_wan.packet_send_req <= 1'b0;
        inst_if_wan.packet_addr <= {my_pkg::PORT_ADDR_LENGTH{1'bz}};
        inst_if_wan.packet_data <= {my_pkg::DATA_WIDTH{1'bz}};
        inst_if_wan.mem_port_index <= {$clog2(my_pkg::NUM_OF_PORTS){1'bz}};
        inst_if_wan.mem_write_enable <= 1'b0;


        @(posedge inst_if_wan.clk);
      end

      // zadost o dalsi polozku ze sekvenceru
      seq_item_port.get_next_item(req);
      // buzeni DUT - zpravidla v samostatnem tasku, ale neni nutne
      drive_item(req);
      // ohlaseni dokonceni buzeni
      seq_item_port.item_done();

      // nechat viset hodnoty na sbernici az do dalsiho taktu hodin
      @(posedge inst_if_wan.clk);

    end
  endtask
  
  task drive_item (my_pkg::my_item req);
      // nastaveni polozek (jednotlivych signalu) rozhrani DUT

    if(req.mem_operation) begin
      inst_if_wan.packet_addr <= req.addr;
      inst_if_wan.mem_port_index <= req.port_index;
      inst_if_wan.mem_write_enable <= 1'b1;

      inst_if_wan.packet_data <= 0;
      inst_if_wan.packet_send_req <= 0;

      @(posedge inst_if_wan.clk)

      inst_if_wan.mem_write_enable <= 1'b0;

    end
    else begin
      inst_if_wan.packet_addr <= req.addr;
      inst_if_wan.packet_data <= req.data;
      inst_if_wan.packet_send_req <= 1'b1;
      
      inst_if_wan.mem_port_index <= 0;
      inst_if_wan.mem_write_enable <= 0;
      
      if (req.mem_write_while_send)
      begin
        @(posedge inst_if_wan.clk)

        //port index does not matter, the operation should be rejected 
        inst_if_wan.mem_port_index <= 0;
        inst_if_wan.mem_write_enable <= 1;

        @(posedge inst_if_wan.clk)

        inst_if_wan.mem_write_enable <= 0;

      end


      // pockat na reakci DUT
      @(posedge inst_if_wan.packet_finished);
      inst_if_wan.packet_send_req <= 1'b0;
      
    end

      // vypis hlasky na konzoli
      //`uvm_info(get_type_name(), $sformatf("Driver: item sent: \n%s", req.sprint()), UVM_LOW);
  endtask

endclass
