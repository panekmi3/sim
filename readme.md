# NI-SIM semestral work

## Task1

Implement a simple packet switch. The switch sends packets from WAN port to the LAN port with address that matches the address requested by the sender. The addresses of individual LAN ports are stored in memory. The switch waits for an acknowledgment from LAN port. No two LAN ports can share an address and the address can't be written while switch is in process of sending a packet.

Write a simple testbench to test all of the switch's features.

## Task2

Use UVM for switch verification.