module switch_tb;

  `define WAIT_CLK(CYCLES) repeat(CYCLES) @(posedge clk);
  
  /*
  verbosity level:
  0 - only pass/fail messages printed
  1 - detailed test prints
  2 - detailed message/memory prints
  */
  localparam VERBOSE = 2;

  //maximum number of clk cycles after packet is finished
  localparam MAX_WAIT = 20;

  // Parameters
  localparam  NUM_OF_PORTS = 5;
  localparam  PORT_ADDR_LENGTH = 3;
  localparam  DATA_WIDTH = 8;

  localparam CLK_PERIOD = 10;

  //Ports 
  reg  clk;
  reg  reset;

  reg [DATA_WIDTH - 1 : 0]                  packet_data, packet_data_correct;
  reg   [PORT_ADDR_LENGTH - 1 : 0]          packet_addr;
  reg                                       packet_send_req;
  reg   [$clog2(NUM_OF_PORTS) - 1 : 0]      mem_port_index, mem_port_index_correct;
  reg                                       mem_write_enable;
  wire                                      packet_finished;
  wire  [NUM_OF_PORTS - 1 : 0]              port_req;
  wire  [NUM_OF_PORTS*DATA_WIDTH - 1 :0 ]   port_data;
  reg  [NUM_OF_PORTS - 1 : 0]               port_received;

  reg  [3:0]                                test_number;

  switch # (
    .NUM_OF_PORTS(NUM_OF_PORTS),
    .PORT_ADDR_LENGTH(PORT_ADDR_LENGTH),
    .DATA_WIDTH(DATA_WIDTH)
  )
  DUT (
    .clk(clk),
    .reset(reset),
    .packet_data(packet_data),
    .packet_addr(packet_addr),
    .packet_send_req(packet_send_req),
    .mem_port_index(mem_port_index),
    .mem_write_enable(mem_write_enable),
    .packet_finished(packet_finished),
    .port_req(port_req),
    .port_data(port_data),
    .port_received(port_received)
  );

  //waits until posedge or specified number of cycles
  //modified from: https://stackoverflow.com/questions/12370324/verilog-equivalent-of-wait-until-for
  task wait_or_timeout;
    input integer cycles;
    begin
      fork : _wait_or_timeout
      begin
        `WAIT_CLK(MAX_WAIT)
        disable _wait_or_timeout;
      end
      begin
        @(posedge packet_finished);
        disable _wait_or_timeout;
      end
      join
    end
  endtask

  task write_address;
    input [$clog2(NUM_OF_PORTS) - 1 : 0] index;
    input [PORT_ADDR_LENGTH - 1 : 0] address;
    begin

      if (VERBOSE >= 2) $display("Writing address: %b to port index: %b", address, index);

      mem_port_index = index;
      packet_addr = address;
      mem_write_enable = 1;
      `WAIT_CLK(1)
      mem_write_enable = 0;
    end
  endtask

  task send_packet;
    input [PORT_ADDR_LENGTH - 1 : 0] address;
    input [DATA_WIDTH - 1 : 0] data;
    begin

      packet_data = data;
      packet_addr = address;
      packet_send_req = 1;

      `WAIT_CLK(1)

      if (DUT.port_found)
        if (VERBOSE >= 2) $display("Sent data: 0x%02H to port address: %b", data, address);
      else
        if (VERBOSE >= 2) $display("Port with address: %b not found!", address);

      packet_send_req <= 0;
    end
  endtask
  
  //TEST1: basic test, setup two ports and try to send data to one of them
  task TEST1;
  begin: _TEST1
  integer i;
  test_number = 1;
  port_received = 0;
  $display("----------------TEST 1----------------");
  write_address(0, 3'b101);
  //copy of port index used for checking later on
  mem_port_index_correct = 0;
  write_address(1, 3'b100);

  send_packet(3'b101, 8'hDE);
  packet_data_correct = 8'hDE;
  
  //wait for controller to send req
  `WAIT_CLK(1)

  for (i=0; i<NUM_OF_PORTS; i=i+1) begin
    if (VERBOSE > 0) $display("Port #%1d addr: %b, data: 0x%02H, port_req[%1d]: %b", i, DUT.data[PORT_ADDR_LENGTH*i +: PORT_ADDR_LENGTH], port_data[DATA_WIDTH*i +: DATA_WIDTH], i, port_req[i]);
    //when the right port is found check that it has the correct data and correct port_req is set
    if (i == mem_port_index_correct) begin
      if (port_data[DATA_WIDTH*i +: DATA_WIDTH] != packet_data_correct || port_req[i] != 1) begin
        $display("Test 1 fail:\nport #%d, data 0x%02H, expected 0x%02H; port_req %b, expected %b", i, port_data[DATA_WIDTH*i +: DATA_WIDTH], packet_data_correct, port_req[i], 1);
        disable _TEST1;
      end
      //the port is correct, send confirmation after random delay
      `WAIT_CLK($random%5+2)
      port_received[i] = 1;

      `WAIT_CLK(1)
      
      if (! packet_finished) begin
        $display("Test 1 failed. Failed to confirm packet completion.");
        disable _TEST1;
      
      port_received[i] = 0;
    end 
  end
  
  end

  $display("Test 1 pass.");
  end
  endtask

  //TEST2: same as test 1 but try to confirm from wrong port
  task TEST2;
  begin: _TEST2
  integer i;
  test_number = 2;
  port_received = 0;
  $display("----------------TEST 2----------------");
  write_address(0, 3'b101);
  //copy of port index used for checking later on
  mem_port_index_correct = 0;
  write_address(1, 3'b100);
  

  send_packet(3'b101, 8'hAD);
  packet_data_correct = 8'hAD;
  
  //wait for controller to send req
  `WAIT_CLK(1)

  for (i=0; i<NUM_OF_PORTS; i=i+1) 
  begin
    if (VERBOSE > 0) 
      $display("Port #%1d addr: %b, data: 0x%02H, port_req[%1d]: %b", i, DUT.data[PORT_ADDR_LENGTH*i +: PORT_ADDR_LENGTH], port_data[DATA_WIDTH*i +: DATA_WIDTH], i, port_req[i]);
    //when the right port is found check that it has the correct data and correct port_req is set
    if (i == mem_port_index_correct) begin
      if (port_data[DATA_WIDTH*i +: DATA_WIDTH] != packet_data_correct || port_req[i] != 1) begin
        $display("Test 2 fail:\nPort #%d, data 0x%02H, expected 0x%02H; port_req %b, expected %1b", i, port_data[DATA_WIDTH*i +: DATA_WIDTH], packet_data_correct, port_req[i], 1);
        disable _TEST2;
      end
      //the port is correct, send confirmation from a different port
      `WAIT_CLK(2)
      port_received[(i + 1)%NUM_OF_PORTS] = 1;
      `WAIT_CLK(1)
      port_received[(i + 1)%NUM_OF_PORTS] = 0;

      if (packet_finished) 
      begin
        $display("Test 2 fail:\nConfirmed from port #%1d, target port was #%1d", (i + 1)%NUM_OF_PORTS, mem_port_index_correct);
        disable _TEST2;
      end

      //confirm from correct port to clear the request
      `WAIT_CLK(2)
      port_received[i] = 1;
      `WAIT_CLK(1)

      if (! packet_finished) begin
        $display("Test 2 failed. Failed to confirm packet completion.");
        disable _TEST2;
      end
      
      port_received[i] = 0;

    end
  end

  $display("Test 2 pass");
  end
  endtask

  //TEST3: Try to send a packet to a nonexistent address
  task TEST3;
    begin: _TEST3
      integer i;
      test_number = 3;
      port_received = 0;
      $display("----------------TEST 3----------------");
      
      //address 001 was not previously set
      send_packet(3'b001, 8'hBE);

      wait_or_timeout(MAX_WAIT);

      if (! packet_finished) 
      begin
        $display("Test 3 failed. Failed to confirm packet completion.");
        disable _TEST3;
      end
      `WAIT_CLK(1)

      //check all the ports to make sure none received any data
      for (i=0; i<NUM_OF_PORTS; i=i+1) 
      begin
        if (port_req[i] != 0) begin
          $display("Test 3 failed. Port[%1d] received data.", i);
          disable _TEST3;
        end
      end

      $display("Test 3 pass.");
    end
  endtask

  //try to write to memory before packet is finished
  task TEST4;
    begin: _TEST4
    integer i;
    reg [NUM_OF_PORTS * PORT_ADDR_LENGTH - 1:0] m_data;
    test_number = 4;
    port_received = 0;
    $display("----------------TEST 4----------------");
    
    //obtain a copy of stored data and send a packet
    m_data = DUT.data;
    send_packet(3'b101, 8'hEF);
    
    //before the packet is finished try to write into memory
    write_address(4, 3'b111);
    
    `WAIT_CLK(2)

    //check if the stored data changed
    if(m_data != DUT.data)
    begin
      $display("Test 4 fail. Data in memory changed.");
      disable _TEST4;
    end

    //confirm the packet
    `WAIT_CLK(2)
    port_received[0] = 1;
    `WAIT_CLK(1)
    port_received[0] = 0;

    $display("Test 4 pass.");

    end  
  endtask

  //try to write duplicate addresses into memory
  task TEST5;
    begin: _TEST5
    integer i;
    reg [NUM_OF_PORTS * PORT_ADDR_LENGTH - 1:0] m_data;
    test_number = 5;
    port_received = 0;
    $display("----------------TEST 5----------------");
    
    //obtain a copy of stored data and try to store an entry with an already existing address
    m_data = DUT.data;    
    write_address(4, 3'b100);
    
    `WAIT_CLK(2)

    //check if the stored data changed
    if(m_data != DUT.data)
    begin
      $display("Test 5 fail. Data in memory changed.");
      disable _TEST5;
    end

    $display("Test 5 pass.");

    end  
  endtask
//main process
initial begin

  //wait until reset is done
  @(posedge reset);
  @(negedge reset);
  `WAIT_CLK(2)

  $display("\n-----------SIMULATION START-----------");
  TEST1();
  TEST2();
  TEST3();
  TEST4();
  TEST5();
  $display("-----------SIMULATION  END-----------");

  `WAIT_CLK(10)

  $finish;

end

always #(CLK_PERIOD/2) clk=~clk;

initial begin
    $dumpfile("switch_tb.vcd");
    $dumpvars(0, switch_tb);
end

//value initialization and reset
initial begin
    packet_send_req = 0;
    packet_addr = 0;
    packet_data = 0;

    port_received = 0;

    mem_write_enable = 0;
    mem_port_index = 0;

    test_number = 0;

    reset <= 0;
    clk <= 0;

    @(posedge clk);
    reset <= 1;
    `WAIT_CLK(2)
    reset <= 0;
end

endmodule