module controller #(
    parameter NUM_OF_PORTS = 5, PORT_ADDR_LENGTH = 3, DATA_WIDTH = 8
)
(
    input wire clk, reset,
    input wire packet_send_req,
    input wire port_found,
    input wire received,

    output reg packet_finished,
    output reg packet_send,
    output reg ready
);

parameter WAIT = 0, LOOK_UP = 1, SEND = 2;

reg [1:0] state = WAIT, next_state;

always @(*) begin
    case (state)
        WAIT: 
            if (packet_send_req)
                next_state <= LOOK_UP;
            else
                next_state <= WAIT;
        LOOK_UP:
            if(port_found)
                next_state <= SEND;
            else
                next_state <= WAIT;
        SEND:
            if(received)
                next_state <= WAIT;
            else
                next_state <= SEND;
        default:
            $display("STATE NOT DEFINED");
    endcase
end

always @(*) begin
    
    packet_finished = 1'b0;
    packet_send = 1'b0;
    ready = 1'b0;

    case (state)
        WAIT:
            ready = 1'b1;
        LOOK_UP:
            if(!port_found)
                packet_finished = 1'b1;
            else
                packet_send = 1'b1;
        SEND:
            if(received)
                packet_finished = 1'b1;
        default:
            $display("STATE NOT DEFINED");
    endcase
end

always @(posedge clk ) begin
    if (reset) 
        state <= WAIT;
    else
        state <= next_state;
end

endmodule