module mux_demux_logic
#(
    parameter NUM_OF_PORTS = 5, PORT_ADDR_LENGTH = 3, DATA_WIDTH = 8
)
(
    input wire clk, reset,
    input wire [DATA_WIDTH - 1 : 0] packet_data,
    input wire packet_send,
    input wire [NUM_OF_PORTS - 1 : 0] eq,
    input wire [NUM_OF_PORTS - 1 : 0] port_received,

    output reg received,
    output reg [NUM_OF_PORTS - 1 : 0] port_req,
    output reg [NUM_OF_PORTS * DATA_WIDTH - 1 : 0] port_data
);

reg [NUM_OF_PORTS - 1 : 0] mask;

integer i;
always @(*) begin

    port_data = 0;
    port_req = 0;

    for (i = 0; i < NUM_OF_PORTS; i = i + 1) begin
        if (eq[i]) begin
            port_data[i * DATA_WIDTH +: DATA_WIDTH] = packet_data;
            port_req[i] = packet_send;
        end 
    end

    received <= | (port_received & eq);
end


endmodule