module switch 
#(
    parameter NUM_OF_PORTS = 5, PORT_ADDR_LENGTH = 3, DATA_WIDTH = 8
)
(
    input wire clk, reset,
    
    input wire [DATA_WIDTH - 1 : 0] packet_data,
    input wire [PORT_ADDR_LENGTH - 1 : 0] packet_addr,
    input wire packet_send_req,

    input wire [$clog2(NUM_OF_PORTS) - 1 : 0] mem_port_index,
    input wire mem_write_enable,

    output wire packet_finished,
    
    output wire [NUM_OF_PORTS-1:0] port_req,
    output wire [NUM_OF_PORTS*DATA_WIDTH-1:0] port_data,
    input wire [NUM_OF_PORTS-1:0] port_received
);

wire port_found, received, packet_send, ready;
wire [NUM_OF_PORTS - 1 : 0] eq;
wire [NUM_OF_PORTS * PORT_ADDR_LENGTH - 1:0] data;

assign port_found = |eq;

controller # (
    .NUM_OF_PORTS(NUM_OF_PORTS),
    .PORT_ADDR_LENGTH(PORT_ADDR_LENGTH),
    .DATA_WIDTH(DATA_WIDTH)
  )
  controller_inst (
    .clk(clk),
    .reset(reset),
    .packet_send_req(packet_send_req),
    .port_found(port_found),
    .received(received),
    .packet_finished(packet_finished),
    .packet_send(packet_send),
    .ready(ready)
  );

memory # (
    .NUM_OF_PORTS(NUM_OF_PORTS),
    .PORT_ADDR_LENGTH(PORT_ADDR_LENGTH),
    .DATA_WIDTH(DATA_WIDTH)
  )
  memory_inst (
    .clk(clk),
    .reset(reset),
    .port_index(mem_port_index),
    .port_addr(packet_addr),
    .write_enable(mem_write_enable),
    .data(data),
    .ready(ready),
    .port_found(port_found)
  );

comparators # (
    .NUM_OF_PORTS(NUM_OF_PORTS),
    .PORT_ADDR_LENGTH(PORT_ADDR_LENGTH),
    .DATA_WIDTH(DATA_WIDTH)
  )
  comparators_inst (
    .packet_addr(packet_addr),
    .mem_data_all(data),
    .eq(eq)
  );

mux_demux_logic # (
    .NUM_OF_PORTS(NUM_OF_PORTS),
    .PORT_ADDR_LENGTH(PORT_ADDR_LENGTH),
    .DATA_WIDTH(DATA_WIDTH)
  )
  mux_demux_logic_inst (
    .clk(clk),
    .reset(reset),
    .packet_data(packet_data),
    .packet_send(packet_send),
    .eq(eq),
    .port_received(port_received),
    .received(received),
    .port_req(port_req),
    .port_data(port_data)
  );




endmodule