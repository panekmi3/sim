module counter (
    input wire clk, reset,
    output reg [31:0] out
);

always @(posedge clk ) begin
    if (reset==1)
        out <= 0;
    else
        out <= out + 1;
end
    
endmodule