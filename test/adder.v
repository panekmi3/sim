module adder (
    output integer sum,
    input wire signed [31:0] a, b
);

always @(*) begin
    sum = a + b;
end

endmodule