`include "adder.v"
`include "counter.v"
`default_nettype none

module tb_adder;
reg clk;
reg rst_n;

wire signed [31:0] a, b, c;

//reg [31:0] out;

adder DUT
(
    c, a, b
);

counter  counter_inst (
    .clk(clk),
    .reset(rst_n),
    .out(b)
  );

localparam CLK_PERIOD = 10;
always #(CLK_PERIOD/2) clk=~clk;

initial begin
    $dumpfile("tb_adder.vcd");
    $dumpvars(0, tb_adder);
end

initial begin
    rst_n <= 0;
    clk <= 0;
    @(posedge clk);
    rst_n <= 1;
    repeat(2) @(posedge clk);
    rst_n <= 0;
    repeat(60) @(posedge clk);
    rst_n <= 1;
    repeat(2) @(posedge clk);
    rst_n <= 0;
end

initial begin
    @(negedge rst_n);
    repeat(100) @(posedge clk);
    $finish(2);
end

assign a = 32'd52;

endmodule
`default_nettype wire